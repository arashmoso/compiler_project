/**
 * Created by A.M.Sohi on 02/09/2017.
 */
public class Main {

    public static void main(String[] args) {
        System.out.println(TType.INT.getClass());
    }
}

enum TType {
    INT,
    FLOAT,
    ID,
    CHAR,
    STRING,
    BOOL,
    EOF,
    SYNTAX,
    OPERATOR,
    KEYWORD,
    HEX,
}