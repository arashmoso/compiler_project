
/**
 * Created by Shervin on 2/8/2017.
 */
public class Token {
    String type;
    String value;

    public Token(String type, String value) {
        this.type = type;
        this.value = value;
    }
}